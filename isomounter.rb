#!/usr/bin/env ruby

require 'fileutils'

$stdout.sync = 1

iso_dir		= 'pv/isos'
disc_dir	= 'pv/discs'

last_mtime = Time.at(0); entries = {}
loop {
	unless((cur_mtime = File.stat(iso_dir).mtime) == last_mtime)
		Dir.open(iso_dir) {|dh|
			dh.each {|de|
				de =~ /(.+)\.iso$/ or next
				entries[disc_name = $1] and next

				puts('mkdir %s' % ['%s/%s' % [disc_dir, disc_name]])
				Dir.mkdir(mount_point = '%s/%s' % [disc_dir, disc_name]) rescue true

				puts('sudo mount -o loop %s/%s %s' % [iso_dir, de, mount_point])
				system('sudo mount -o loop %s/%s %s' % [iso_dir, de, mount_point])
				(it = $?.exitstatus and it == 0) or puts('Use the mkloopdev.rb, Luke.')

				entries[$1] = true
			}
		}
		last_mtime = cur_mtime
	end
	sleep(5)
}

__END__

