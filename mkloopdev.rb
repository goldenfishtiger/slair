#!/usr/bin/env ruby

# コンテナを立ち上げる前に、コンテナの外で実行する

cmd = {
	:LOOP_CTL_ADD		=> 0x4C80,
	:LOOP_CTL_REMOVE	=> 0x4C81,
	:LOOP_CTL_GET_FREE	=> 0x4C82,
}

create_max = 20; remove = false
ARGV.each {|arg|
	arg =~ /^\d/ and create_max = arg.to_i
	arg =~ /^-r/ and remove = true
}

open('/dev/loop-control', 'w') {|fd|
	create_max.times {|n|
		begin
			unless(remove)
				res = fd.ioctl(cmd[:LOOP_CTL_ADD], n)
				puts('/dev/loop%d created.[res = %d]' % [n, res])
			else
				res = fd.ioctl(cmd[:LOOP_CTL_REMOVE], n)
				puts('/dev/loop%d removed.[res = %d]' % [n, res])
			end
		rescue
			p $!
		end
	}
}

__END__

