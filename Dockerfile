FROM goldenfishtiger/sinatra_skelton

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

USER root
WORKDIR /

# https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921
#SHELL ["/clone3-workaround", "/bin/bash", "-c"]

# git clone slair しておくこと
ARG TARGET=slair
RUN set -x \
	&& ln -s /home/user/sinatra_skelton /home/user/${TARGET}
WORKDIR /home/user/${TARGET}
COPY ${TARGET} /home/user/${TARGET}

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	CONFIG=slair.config
#	if [ ! -e pv/$CONFIG ]; then
#		echo "# $CONFIG.sample set up."
#		cp -av $CONFIG.sample pv
#		echo "Rename 'pv/$CONFIG.sample' to 'pv/$CONFIG' and modify it."
#		echo '**** HALT ****'
#		sleep infinity
#	fi
#
#	if [ -e pv/dot.bashrc ]; then
#		cp -av pv/dot.bashrc /home/user/.bashrc
#		cp -av pv/dot.virc /home/user/.virc
#		cp -av pv/dot.gitconfig /home/user/.gitconfig
#	fi
#
#	if [ ! -e pv/isos/PUT_THE_ISO_HERE ]; then
#		mkdir -pv pv/isos
#		mkdir -pv pv/discs
#		touch pv/isos/PUT_THE_ISO_HERE
#	fi
#
#	rm -v public/indexes
#	mkdir -v public/indexes
#	ln -sv ../../pv/isos public/indexes
#	ln -sv ../../pv/discs public/indexes
#
#	# easy cron
#	now=`date +%s`
#	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 5 * 60 +  4 * 3600 + 0 * 86400 - 378000))}))	# week
#	while [ $target0 -lt $now ]; do
#		((target0 += CYCLE0))
#	done
#
#	s=S
#	while true; do
#		pgrep -f puma > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart puma."
#			bundle exec rackup -P /tmp/rack.pid --host 0.0.0.0 --port 8080 &
#		fi
#		pgrep -f isomounter.rb > /dev/null
#		if [ $? -ne 0 ]; then
#			echo "`date`: ${s}tart isomounter."
#			./isomounter.rb &
#		fi
#		s=Res
#
#		# easy cron
#		if [ `date +%s` -ge $target0 ]; then
#			((target0 += CYCLE0))
#			echo "`date`: Job easy cron 0 started."
#			target_logs="pv/puma.std???"
#			gen1=${ROTGENS:=4}; while true; do
#				gen0=$((gen1-1))
#				_gen0=.$gen0; _gen1=.$gen1
#				[ $gen0 -eq 0 ] && _gen0=
#				for target_log in $target_logs; do
#					mv -v $target_log$_gen0 $target_log$_gen1
#				done
#				gen1=$gen0; [ $gen1 -eq 0 ] && break
#			done
#			pkill -HUP -F /tmp/rack.pid
#		fi
#		sleep 5
#	done
#
##	target0=$((now - now % ${CYCLE0:=900}     + ${SCHED0:=$(( 5 * 60))}))									# 15min
##	target0=$((now - now % ${CYCLE0:=3600}    + ${SCHED0:=$(( 0 * 60))}))									# hour
##	target0=$((now - now % ${CYCLE0:=86400}   + ${SCHED0:=$(( 0 * 60 +  0 * 3600 - 32400))}))				# day
##	target0=$((now - now % ${CYCLE0:=604800}  + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# week
##	target0=$((now - now % ${CYCLE0:=2419200} + ${SCHED0:=$(( 0 * 60 +  0 * 3600 + 0 * 86400 - 378000))}))	# 4weeks
#
##__END0__startup.sh__

#USER user
USER root

ENTRYPOINT ["bash", "-c"]
CMD ["bash startup.sh"]

